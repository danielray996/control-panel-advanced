<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <nav class="navbar align-items-center">
                {{ __('Customer') }}
                <a href="\"><i class="fas fa-arrow-left text-blue"></i></a>
            </nav>
        </h2>
    </x-slot>
    <?php if(!empty($users)) {?>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="table">
                        <tr>
                            <th>Nome</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Options</th>
                        </tr>
                        @forEach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->password}}</td>
                                <td><a href="/dashboard/{{ $user->id }}" class="btn btn-primary mx-2">Modifica</a>
                                <a href="/dashboard/delete/{{ $user->id }}" class="btn btn-danger">Elimina</a></td>
                            </tr>
                        @endforeach
                    </table>
                    <nav class="navbar align-items-center">
                    {{ $users->links() }}
                        <button id="buttonreg" class="btn btn-outline-dark">Add new +</button>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if(!empty($utente)) {?>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="" method="POST">
                        <div class="text-center"><strong>Aggiorna utente</strong></div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <p>
                        <label for="name"><strong>Nome</strong></label>
                        <input id="name" class="form-control mb-2" type="text" name="name" value="{{$utente->name}}">
                        </p>
                        <p>
                        <label for="email"><strong>Email<strong></label>
                        <input id="email" class="form-control mb-2" type="text" name="email" value="{{$utente->email}}">
                        </p>
                        <p>
                        <label for="password"><strong>Password</strong></label>
                        <input id="password" class="form-control mb-3" type="text" name="password" value="{{$utente->password}}">
                        </p>
                        <div class="text-center"><input type="submit" class="btn btn-success" value="Aggiorna utente"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div id="register" class="py-12 d-none">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 bg-white border-b border-gray-200">
                        <form action="" method="POST">
                            <div class="text-center"><strong>Crea utente</strong></div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <p>
                            <label for="name"><strong>Nome</strong></label>
                            <input id="name" class="form-control mb-2" type="text" name="name" placeholder="Nome">
                            </p>
                            <p>
                            <label for="email"><strong>Email<strong></label>
                            <input id="email" class="form-control mb-2" type="text" name="email" placeholder="Email">
                            </p>
                            <p>
                            <label for="password"><strong>Password</strong></label>
                            <input id="password" class="form-control mb-3" type="text" name="password" placeholder="Password">
                            </p>
                            <div class="text-center"><input type="submit" class="btn btn-success" value="Crea utente"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <script>
        let reg = document.getElementById('register');
        let buttonreg = document.getElementById('buttonreg')
        var toggle = false;
        buttonreg.addEventListener('click', function(e) {
            e.preventDefault();
            if(!toggle){
                reg.classList.remove('d-none');
            }else{
                reg.classList.add('d-none');
            }
            toggle = !toggle;
            console.log(toggle)
        })
    </script>
</x-app-layout>
