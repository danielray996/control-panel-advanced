<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UtentiController; 
use App\Http\Controllers\Auth\AuthenticatedSessionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Login



//Dashboard & CRUD
Route::redirect('/', '/login');
Route::get('/dashboard', [UtentiController::class, 'index'])->middleware(['auth'])->name('dashboard');
Route::post('/dashboard', [UtentiController::class, 'create'])->middleware(['auth']);
Route::get('/dashboard/{id}', [UtentiController::class, 'editRead'])->middleware(['auth']);
Route::post('/dashboard/{id}', [UtentiController::class, 'edit'])->middleware(['auth']);
Route::get('/dashboard/delete/{id}', [UtentiController::class, 'destroy'])->middleware(['auth']);

//Logout
Route::get('/logout', [AuthenticatedSessionController::class, 'destroy']);

require __DIR__.'/auth.php';
