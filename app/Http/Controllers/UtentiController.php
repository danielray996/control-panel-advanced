<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UtentiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $utenti = User::orderby('id','ASC')->simplepaginate(2);
        return view('dashboard',['users'=>$utenti]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $test = User::where('email', '=', $request->email)->get();
        if(count($test) == 0){
            $anagrafica = new User;
            $anagrafica->name = $request->name;
            $anagrafica->password = Hash::make($request->password);
            $anagrafica->email = $request->email;
            $anagrafica->save();

            return redirect('dashboard');
        } else {
            $errore = '';
            return view('dashboard', compact('errore'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editRead($id)
    {
        $item = User::find($id);
        return view('dashboard', ['utente'=>$item]);
    }
    public function edit(Request $request, $id)
    {

        $item = User::find($id);
        $item->name = $request->name;
        $item->password = Hash::make($request->password);
        $item->email = $request->email;
        $item->save();
        return redirect('/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect('/dashboard');
    }
}
